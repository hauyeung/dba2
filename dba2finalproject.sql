drop database if exists wholesaleStore;
drop user 'gtrainor'@'localhost';
drop user 'rlafrance'@'localhost';
drop user 'stomson'@'localhost';
drop user 'sarpin'@'localhost';
drop user 'ttyre'@'localhost';
create database wholesaleStore;
use wholesaleStore;
create table Customers(CustomersId int not null primary key auto_increment,
	firstName varchar(500),
	lastName varchar(500),
	phoneNumber varchar(500),
	address varchar(500),
	city varchar(500),
	region varchar(100),
	country varchar(200),
	postalCode varchar(20),
	faxNumber varchar(20),
	emailAddress varchar(100)
);

create table Employees(EmployeesId int not null primary key auto_increment,
	firstName varchar(500),
	lastName varchar(500),
	phoneNumber varchar(500),
	address varchar(500),
	city varchar(500),
	region varchar(100),
	country varchar(200),
	postalCode varchar(20),
	faxNumber varchar(20),
	emailAddress varchar(100),
	employeeType varchar(200),
	userName varchar(200)
);
create table Products (ProductsId int not null primary key auto_increment,
	name varchar(500),
	description text,
	SuppliersId int,
	foreign key (SuppliersId) references Suppliers(SuppliersId)
);

create table Suppliers (SuppliersId int not null primary key auto_increment,
	name varchar(500),
	phoneNumber varchar(500),
	address varchar(500),
	city varchar(500),
	region varchar(100),
	country varchar(200),
	postalCode varchar(20),
	faxNumber varchar(20),
	emailAddress varchar(100)
);

create table Orders (OrdersId int not null primary key auto_increment,
	orderDate datetime,
	CustomersId int,
	quantity int,
	CashiersId int,
	ProductsId int,
	EmployeesId int,
	foreign key (CustomersId) references Customers(CustomersId),
	foreign key (EmployeesId) references Employees(EmployeesId),
	foreign key (ProductsId) references Products(ProductsId)
);
insert into Customers values ('','Alice','Lisby','(410) 314-1406','8551 Golden Autumn Highway','Kaihon Kug','Maryland','United States','20898-0573',
'(410) 314-1406','x92otkz@ie0kf7.com');
insert into Customers values ('','Hal','Folmar','(706) 512-3600','7193 Middle Crossing','Free Home','Maryland','Georgia','39928-7333','(706) 512-3600','Sorinut9177@teleworm.us');
insert into Customers values ('','Shay','Dandy','(401) 431-2292','6494 Iron Cider By-pass','Nanatlugunyi','Rhode Island','United States','02835-7440','(401) 243-3267','LClady1970@superrito.com');

insert into Employees values ('','Tommie','Tyre','(410) 314-1406','6935 Amber Highway, Wayzata','Kaihon Kug','Maryland','United States','20898-0573',
'(410) 604-9122','ttyre@store.com','cashier','ttyre');
insert into Employees values ('','Sunday','Arpin','(410) 314-1406','5118 Quiet Branch Nook','Kaihon Kug','Maryland','United States','20898-0573','(410) 611-0345','sarpin@store.com','cashier','sarpin');
insert into Employees values ('','Shantell','Tomson','(410) 314-1406','3563 Wishing Apple Key','Kaihon Kug','Maryland','United States','43724-5322',
'(410) 314-1406','stomson@store.com','cashier','stomson');
insert into Employees values ('','Rupert','Lafrance','(410) 314-1406','8551 Golden Autumn Highway','Kaihon Kug','Maryland','United States','43724-1234',
'(410) 225-6629','rlafrance@store.com','manager','rlafrance');
insert into Employees values ('','Georgine','Trainor','(410) 314-1406','2942 Little Arbor','Kaihon Kug','Maryland','United States','43724-0573','(410) 329-8304','gtrainor@store.com','CEO','gtrainor');

insert into Suppliers values ('','Zathlam','(907) 683-3487','1636 Rustic Crescent','Calmar','Alaska','United States','99927-9380','613-555-0181','Shand6005@armyspy.com');
insert into Suppliers values ('','Lexilane','(907) 683-3487','3065 Dewy Bank','Zenon Park','Illinois','United States','62293-0241','(217) 863-1383','Shand6005@armyspy.com');
insert into Suppliers values ('','Planetfan','(907) 683-3487','2943 Harvest Concession','Onalaska','Illinois','United States','60790-2641','(872) 411-8326','Shand6005@armyspy.com');


insert into Products values ('','Chicken','',1);
insert into Products values ('','Beef','',2);
insert into Products values ('','Cucumber','',3);
insert into Products values ('','Watermelon','',2);
insert into Products values ('','Orange','',3);

insert into Orders values ('',str_to_date('May 1, 2014 09:00:01','%M %d,%Y %h:%i:%s'),1,200,1,2,1);
insert into Orders values ('',str_to_date('May 2, 2014 09:00:01','%M %d,%Y %h:%i:%s'),2,300,2,3,3);
insert into Orders values ('',str_to_date('May 1, 2014 09:00:01','%M %d,%Y %h:%i:%s'),3,260,3,1,3);
insert into Orders values ('',str_to_date('May 9, 2014 10:00:01','%M %d,%Y %h:%i:%s'),1,590,2,1,1);
insert into Orders values ('',str_to_date('May 10, 2014 11:00:01','%M %d,%Y %h:%i:%s'),3,100,1,2,1);

create user 'gtrainor'@'localhost';
create user 'rlafrance'@'localhost';
create user 'stomson'@'localhost';
create user 'sarpin'@'localhost';
create user 'ttyre'@'localhost';

create view Orders_Limited as select Orders.OrderDate, Employees.userName from Orders inner join Employees on Orders.EmployeesId = Employees.EmployeesId inner join Products on Products.ProductsId = Orders.ProductsId where trim(substring_index(user(), '@',1)) = trim(Employees.userName);

grant all privileges on Customers to 'gtrainor'@'localhost';
grant all privileges on Products to 'gtrainor'@'localhost';
grant all privileges on Suppliers to 'gtrainor'@'localhost';
grant all privileges on Orders to 'gtrainor'@'localhost';
grant all privileges on Employees to 'gtrainor'@'localhost';

grant all privileges on Products to 'ttyre'@'localhost';
grant all privileges on Orders_Limited to 'ttyre'@'localhost';

grant all privileges on Products to 'sarpin'@'localhost';
grant all privileges on Orders_Limited to 'sarpin'@'localhost';

grant all privileges on Products to 'stomson'@'localhost';
grant all privileges on Orders_Limited to 'stomson'@'localhost';

grant select (firstName, lastName, address, emailAddress, phoneNumber, postalCode), insert, update, delete on Employees to 'rlafrance'@'localhost';
grant select (firstName, lastName, address, emailAddress, phoneNumber, postalCode), insert, update, delete on Customers to 'rlafrance'@'localhost';
grant select (name, phoneNumber, address, region, country, phoneNumber, postalCode), insert, update, delete on Suppliers to 'rlafrance'@'localhost';

flush privileges;

create index orderDt on Orders (orderDate);
create index empUserName on Employees (userName);

optimize table Customers;
optimize table Products;
optimize table Suppliers;
optimize table Orders;
optimize table Employees;

set password for 'ttyre'@'localhost' =  password('password');
set password for 'sarpin'@'localhost' =  password('password');
set password for 'stomson'@'localhost' =  password('password');
set password for 'gtrainor'@'localhost' =  password('password');
set password for 'rlafrance'@'localhost' =  password('password');





